/* eslint-disable react/jsx-props-no-spreading */
import React, { Component } from 'react';
import {
  Form, Button, Input, message,
} from 'antd';
import { get } from 'lodash';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import { FormattedMessage, injectIntl } from 'react-intl';
import locales from '../../utils/i18n/messages/texts';
import { getUser } from '../../utils/http';
import { setLocalStorage } from '../../utils/localeStorage/storage';
import api from '../../utils/directualAPI/api';
import './LoginPage.less';


class LoginPage extends Component {
  state = {
    loading: false,
  }

  onSubmit = (values) => {
    this.setState({ loading: true });
    const { intl } = this.props;
    api.structure().auth(values.username, values.password)
      .then((response) => {
        if (response.isError) {
          if (response.isPassError) {
            message.error(intl.formatMessage({ id: locales.passwordError.id }));
          } else {
            message.error(response.token);
          }
          this.setState({ loading: false });
        } else {
          getUser(values.username, response.token)
            .then((user) => {
              const payload = {
                token: response.token,
                ...user,
                birthdayDate: user.birthdayDate ? user.birthdayDate.toISOString() : '',
              };
              setLocalStorage(payload);
              this.setState({ loading: false });
              message.success(intl.formatMessage({ id: locales.success.id }));
              const { location, history } = this.props;
              const from = get(location, 'state.from.pathname', '/homePage');
              history.push(from);
            });
        }
      })
      .catch((error) => message.error(error.message));
  }

  render() {
    const { loading } = this.state;
    const { intl } = this.props;
    return (
      <div className="loginPage-wrapper">
        <Form
          initialValues={{ remember: true }}
          onFinish={this.onSubmit}
          className="loginPage-form-wrapper"
        >
          <div className="loginPage-form-title-wrapper">
            <div className="loginPage-form-title">
              <span className="loginPage-form-title-text"><FormattedMessage id={locales.enter.id} /></span>
            </div>
            <div className="loginPage-form-title" style={{ cursor: 'not-allowed', marginLeft: '30px', opacity: 0.8 }}>
              <span className="loginPage-form-title-text"><FormattedMessage id={locales.registration.id} /></span>
            </div>
          </div>
          <div className="loginPage-form-input-label"><FormattedMessage id={locales.login.id} /></div>
          <Form.Item
            name="username"
            rules={[{ required: true, message: intl.formatMessage({ id: locales.enterLogin.id }) }]}
          >
            <Input disabled={loading} style={{ backgroundColor: '#fff' }} />
          </Form.Item>
          <div className="loginPage-form-input-label"><FormattedMessage id={locales.password.id} /></div>
          <Form.Item
            name="password"
            rules={[{ required: true, message: intl.formatMessage({ id: locales.enterPassword.id }) }]}
          >
            <Input.Password disabled={loading} />
          </Form.Item>
          <div className="loginPage-form-help">
            <span><FormattedMessage id={locales.restorePass.id} /></span>
          </div>
          <Form.Item>
            <Button type="primary" htmlType="submit" className="loginPage-form-button" disabled={loading}>
              <FormattedMessage id={locales.enter.id} />
            </Button>
          </Form.Item>
        </Form>
      </div>
    );
  }
}

LoginPage.propTypes = {
  location: PropTypes.shape().isRequired,
  history: PropTypes.shape().isRequired,
  intl: PropTypes.shape().isRequired,
};

export default withRouter(injectIntl(LoginPage));
