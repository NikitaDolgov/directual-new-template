/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from 'react';
import { ArrowLeftOutlined } from '@ant-design/icons';
import PropTypes from 'prop-types';
import { FormattedMessage, useIntl } from 'react-intl';
import './SettingsGeneral.less';
import locales from '../../../../utils/i18n/messages/texts';
import { LOCALES } from '../../../../utils/i18n';
import { routes } from '../../../../utils/routes';
import getUserTheme from '../../../../utils/localeStorage/getUserTheme';
import colors from '../../../../assets/styles/colors';
import hexToRgbA from '../../../../utils/hexToRgba';


const SettingsGeneral = ({
  history, language, onChangeLanguage, onChangeTheme, theme,
}) => {
  const intl = useIntl();
  return (
    <div className="settingsGeneral-wrapper">
      <div
        className="settingsGeneral-header"
        style={{
          paddingTop: window.matchMedia('(display-mode: standalone)').matches ? '40px' : '0px',
          height: window.matchMedia('(display-mode: standalone)').matches ? '100px' : '50px',
          backgroundColor: colors[getUserTheme()].elementBackgroundColor,
          color: colors[getUserTheme()].mainTextColor,
        }}
      >
        <div style={{ width: '20px', height: '20px' }} onClick={() => history.push(routes.settings)}>
          <ArrowLeftOutlined />
        </div>
        <span style={{ fontSize: '16px' }}><FormattedMessage id={locales.generalSettings.id} /></span>
        <div />
      </div>
      <div className="settingsMobile-section-wrapper" style={{ backgroundColor: colors[getUserTheme()].elementBackgroundColor, color: colors[getUserTheme()].mainTextColor }}>
        <div className="settingsMobile-section-wrapper-row">
          <select value={language} className="settingsGeneral-select" style={{ backgroundColor: hexToRgbA(colors[getUserTheme()].elementBackgroundColor) }} onChange={(e) => onChangeLanguage(e.target.value)}>
            {[{ id: LOCALES.RUSSIAN, value: 'Русский' }, { id: LOCALES.ENGLISH, value: 'English' }].map((el) => (
              <option key={el.id} value={el.id}>
                {el.value}
              </option>
            ))}
          </select>
        </div>
        <div className="settingsMobile-card-line" />
        <div className="settingsMobile-section-wrapper-row">
          <select value={theme} className="settingsGeneral-select" style={{ backgroundColor: hexToRgbA(colors[getUserTheme()].elementBackgroundColor) }} onChange={(e) => onChangeTheme(e.target.value)}>
            {[{ id: 'light', value: intl.formatMessage({ id: locales.lightTheme.id }) }, { id: 'dark', value: intl.formatMessage({ id: locales.darkTheme.id }) }].map((el) => (
              <option key={el.id} value={el.id}>
                {el.value}
              </option>
            ))}
          </select>
        </div>
      </div>
    </div>
  );
};

SettingsGeneral.propTypes = {
  history: PropTypes.shape().isRequired,
  language: PropTypes.string.isRequired,
  onChangeLanguage: PropTypes.func.isRequired,
  onChangeTheme: PropTypes.func.isRequired,
  theme: PropTypes.string.isRequired,
};

export default SettingsGeneral;

