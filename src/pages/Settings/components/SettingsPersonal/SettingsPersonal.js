/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { Component } from 'react';
import moment from 'moment';
import {
  Input, Form, Avatar, message,
} from 'antd';
import { UserOutlined } from '@ant-design/icons';
import PropTypes from 'prop-types';
import { FormattedMessage, injectIntl } from 'react-intl';
import InputMask from 'react-input-mask';
import locales from '../../../../utils/i18n/messages/texts';
import './SettingsPersonal.less';
import { routes } from '../../../../utils/routes';
import getUserLogo from '../../../../utils/localeStorage/getUserLogo';
import getUserId from '../../../../utils/localeStorage/getUserId';
import getUserFirstName from '../../../../utils/localeStorage/getUserFirstName';
import getUserLastName from '../../../../utils/localeStorage/getUserLastName';
import getUserEmail from '../../../../utils/localeStorage/getUserEmail';
import getUserMiddleName from '../../../../utils/localeStorage/getUserMiddleName';
import getUserBirthdayDate from '../../../../utils/localeStorage/getUserBirthdayDate';
import getUserPhone from '../../../../utils/localeStorage/getUserPhone';
import { editUser } from '../../../../utils/http';
import { setLocalStorage as setStorage } from '../../../../utils/localeStorage/storage';
import DatePicker from '../../../../components/DatePicker/DatePicker';
import getUserTheme from '../../../../utils/localeStorage/getUserTheme';
import colors from '../../../../assets/styles/colors';
import FileUpload from '../../../../components/FileUpload/FileUpload';


class SettingsPersonal extends Component {
  constructor(props) {
    super(props);
    this.formRef = React.createRef();
    this.state = {
      loading: false,
      isShowPhotoUpload: false,
      isShowSelectDate: false,
      birthdayDate: getUserBirthdayDate() ? moment(getUserBirthdayDate()) : null,
    };
  }

  onSubmit = (values) => {
    this.setState({ loading: true });
    const body = {
      id: getUserId(),
      ...values,
      birthdayDate: values.birthdayDate ? values.birthdayDate.toISOString() : null,
    };
    editUser(body)
      .then(() => {
        const { intl } = this.props;
        setStorage(body);
        this.setState({ loading: false });
        message.success(intl.formatMessage({ id: locales.success.id }));
        this.onCancel();
      })
      .catch((error) => {
        this.setState({ loading: false });
        message.error(error.message);
      });
  }

  onCancel = () => {
    const { history } = this.props;
    history.push(routes.settings);
  }

  onShowUpload = (value) => {
    const { onShowBottomMobile } = this.props;
    this.setState({ isShowPhotoUpload: value });
    onShowBottomMobile(!value);
  }

  onShowSelectDate = () => {
    const { onShowBottomMobile } = this.props;
    this.setState({ isShowSelectDate: true });
    onShowBottomMobile(false);
  }

  onFileChange = (file) => {
    const { onShowBottomMobile } = this.props;
    this.setState({ loading: true, isShowPhotoUpload: false });
    onShowBottomMobile(true);
    const headers = {
      'Content-Type': 'multipart/form-data',
    };
    const form = new FormData();
    form.append('id', getUserId());
    form.append('logo_id', file);
    editUser(form, headers)
      .then((user) => {
        setStorage({ logo: user.logoURL });
        this.setState({ loading: false });
      })
      .catch((error) => {
        this.setState({ loading: false });
        message.error(error.message);
      });
  };

  onCancelSelectDate = () => {
    const { onShowBottomMobile } = this.props;
    this.setState({ isShowSelectDate: false });
    onShowBottomMobile(true);
  }

  onConfirmSelectDate = (birthdayDate) => {
    const { onShowBottomMobile } = this.props;
    this.formRef.current.setFieldsValue({
      birthdayDate,
    });
    this.setState({ isShowSelectDate: false, birthdayDate });
    onShowBottomMobile(true);
  }

  render() {
    const {
      loading, isShowPhotoUpload, isShowSelectDate, birthdayDate,
    } = this.state;
    const { intl } = this.props;
    let minHeight = '100vh';
    if (!isShowPhotoUpload && !isShowSelectDate && window.matchMedia('(display-mode: standalone)').matches) {
      minHeight = '100vh';
    }
    if ((isShowPhotoUpload || isShowSelectDate) && window.matchMedia('(display-mode: standalone)').matches) {
      minHeight = '101vh';
    }
    if ((isShowPhotoUpload || isShowSelectDate) && !window.matchMedia('(display-mode: standalone)').matches) {
      minHeight = '85vh';
    }
    return (
      <div className={`settingsPersonal-wrapper ${isShowPhotoUpload || isShowSelectDate ? 'no-scroll' : ''}`} style={{ minHeight }}>
        <Form
          initialValues={{ remember: true }}
          onFinish={this.onSubmit}
          ref={this.formRef}
        >
          <div
            className="settingsPersonal-avatar-wrapper"
            style={{
              backgroundColor: colors[getUserTheme()].elementBackgroundColor,
              paddingTop: window.matchMedia('(display-mode: standalone)').matches ? '50px' : '20px',
            }}
          >
            <button
              type="button"
              onClick={this.onCancel}
              style={{ fontWeight: 'normal' }}
              className="settingsPersonal-button-done"
              disabled={loading}
            >
              <FormattedMessage id={locales.cancel.id} />
            </button>
            <div className="settingsPersonal-avatar-icon-wrapper">
              {getUserLogo()
                ? <Avatar src={getUserLogo()} size={100} />
                : <Avatar style={{ backgroundColor: colors[getUserTheme()].primaryColor }} icon={<UserOutlined />} size={100} />}
              <button
                style={{ marginTop: '10px', fontWeight: 'normal' }}
                className="settingsPersonal-button-done"
                type="button"
                onClick={() => this.onShowUpload(true)}
              >
                <FormattedMessage id={locales.setNewPhoto.id} />
              </button>
            </div>
            <button type="submit" className="settingsPersonal-button-done" disabled={loading}>
              <FormattedMessage id={locales.done.id} />
            </button>
          </div>

          <div className="settingsMobile-section-wrapper" style={{ backgroundColor: colors[getUserTheme()].elementBackgroundColor, color: colors[getUserTheme()].mainTextColor }}>
            <div className="settingsMobile-section-wrapper-row">
              <Form.Item
                name="firstName"
                initialValue={getUserFirstName()}
              >
                <Input
                  placeholder={intl.formatMessage({ id: locales.firstName.id })}
                  disabled={loading}
                />
              </Form.Item>
            </div>
            <div className="settingsMobile-card-line" />
            <div className="settingsMobile-section-wrapper-row">
              <Form.Item
                name="lastName"
                initialValue={getUserLastName()}
              >
                <Input
                  placeholder={intl.formatMessage({ id: locales.lastName.id })}
                  disabled={loading}
                />
              </Form.Item>
            </div>
            <div className="settingsMobile-card-line" />
            <div className="settingsMobile-section-wrapper-row">
              <Form.Item
                name="middleName"
                initialValue={getUserMiddleName()}
              >
                <Input
                  placeholder={intl.formatMessage({ id: locales.middleName.id })}
                  disabled={loading}
                />
              </Form.Item>
            </div>
            <div className="settingsMobile-card-line" />
            <div className="settingsMobile-section-wrapper-row">
              <Form.Item
                name="birthdayDate"
                initialValue={getUserBirthdayDate() ? moment(getUserBirthdayDate()) : null}
              >
                <div
                  onClick={this.onShowSelectDate}
                >
                  {birthdayDate ? birthdayDate.format('DD MMMM YYYY') : ''}
                </div>
              </Form.Item>
            </div>
          </div>
          <div className="settingsMobile-section-hint" style={{ color: colors[getUserTheme()].secondaryTextColor }}>
            <FormattedMessage id={locales.personalDataFIOHint.id} />
          </div>

          <div className="settingsMobile-section-wrapper" style={{ backgroundColor: colors[getUserTheme()].elementBackgroundColor, color: colors[getUserTheme()].mainTextColor }}>
            <div className="settingsMobile-section-wrapper-row">
              <Form.Item
                name="email"
                initialValue={getUserEmail()}
              >
                <Input
                  placeholder={intl.formatMessage({ id: locales.email.id })}
                  disabled={loading}
                />
              </Form.Item>
            </div>
            <div className="settingsMobile-card-line" />
            <div className="settingsMobile-section-wrapper-row">
              <Form.Item
                name="phone"
                initialValue={getUserPhone()}
              >
                <InputMask
                  mask="+7 (999) 999-99-99"
                  maskChar=" "
                  className="settingsPersonal-phone-input"
                  placeholder={intl.formatMessage({ id: locales.phone.id })}
                  disabled={loading}
                  inputMode="numeric"
                />
              </Form.Item>
            </div>
          </div>
          <div className="settingsMobile-section-hint" style={{ color: colors[getUserTheme()].secondaryTextColor }}>
            <FormattedMessage id={locales.personalDataContactHint.id} />
          </div>
        </Form>
        <DatePicker
          visible={isShowSelectDate}
          initialValue={getUserBirthdayDate() ? moment(getUserBirthdayDate()) : moment()}
          onConfirm={(date) => this.onConfirmSelectDate(date)}
          onCancel={() => this.onCancelSelectDate()}
        />
        <FileUpload
          visible={isShowPhotoUpload}
          onFileChange={(f) => this.onFileChange(f)}
          onCancel={() => this.onShowUpload(false)}
        />
      </div>
    );
  }
}

SettingsPersonal.propTypes = {
  history: PropTypes.shape().isRequired,
  intl: PropTypes.shape().isRequired,
  onShowBottomMobile: PropTypes.func.isRequired,
};

export default injectIntl(SettingsPersonal);
