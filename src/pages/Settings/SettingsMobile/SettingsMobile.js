/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Badge } from 'antd';
import {
  UserOutlined,
  SettingOutlined,
  RightOutlined,
  LogoutOutlined,
  BellOutlined,
  MessageOutlined,
} from '@ant-design/icons';
import locales from '../../../utils/i18n/messages/texts';
import './SettingsMobile.less';
import { routes } from '../../../utils/routes';
import logout from '../../../utils/logout';
import getUserTheme from '../../../utils/localeStorage/getUserTheme';
import colors from '../../../assets/styles/colors';


const SettingsMobile = ({ history }) => {
  const onRowClick = (path) => () => history.push(path);

  return (
    <div className="settingsMobile-wrapper">
      <div
        className="settingsMobile-title"
        style={{
          paddingTop: window.matchMedia('(display-mode: standalone)').matches ? '40px' : '0px',
        }}
      >
        <span style={{ color: colors[getUserTheme()].mainTextColor }}>
          <FormattedMessage id={locales.settings.id} />
        </span>
      </div>

      <div className="settingsMobile-section-wrapper" style={{ backgroundColor: colors[getUserTheme()].elementBackgroundColor, color: colors[getUserTheme()].mainTextColor }}>
        <div className="settingsMobile-section-wrapper-row" onClick={onRowClick(routes.settingsPersonal)}>
          <div>
            <UserOutlined className="settingsMobile-section-wrapper-row-icon" />
            <span className="settingsMobile-section-wrapper-row-text">
              <FormattedMessage id={locales.personalData.id} />
            </span>
          </div>
          <RightOutlined className="settingsMobile-section-wrapper-row-left-icon" />
        </div>
      </div>

      <div className="settingsMobile-section-wrapper" style={{ backgroundColor: colors[getUserTheme()].elementBackgroundColor, color: colors[getUserTheme()].mainTextColor }}>
        <div className="settingsMobile-section-wrapper-row" onClick={() => {}}>
          <div>
            <MessageOutlined className="settingsMobile-section-wrapper-row-icon" />
            <span className="settingsMobile-section-wrapper-row-text">
              <FormattedMessage id={locales.help.id} />
            </span>
          </div>
          <RightOutlined className="settingsMobile-section-wrapper-row-left-icon" />
        </div>
        <div className="settingsMobile-card-line" style={{ borderColor: colors[getUserTheme()].secondaryTextColor }} />
        <div className="settingsMobile-section-wrapper-row" onClick={onRowClick(routes.settingsGeneral)}>
          <div>
            <SettingOutlined className="settingsMobile-section-wrapper-row-icon" />
            <span className="settingsMobile-section-wrapper-row-text">
              <FormattedMessage id={locales.settings.id} />
            </span>
          </div>
          <RightOutlined className="settingsMobile-section-wrapper-row-left-icon" />
        </div>
        <div className="settingsMobile-card-line" style={{ borderColor: colors[getUserTheme()].secondaryTextColor }} />
        <div className="settingsMobile-section-wrapper-row">
          <div>
            <Badge dot offset={[-14, 3]}>
              <BellOutlined className="settingsMobile-section-wrapper-row-icon" style={{ color: colors[getUserTheme()].mainTextColor }} />
            </Badge>
            <span className="settingsMobile-section-wrapper-row-text">
              <FormattedMessage id={locales.notifications.id} />
            </span>
          </div>
          <RightOutlined className="settingsMobile-section-wrapper-row-left-icon" />
        </div>
      </div>

      <div className="settingsMobile-section-wrapper" style={{ backgroundColor: colors[getUserTheme()].elementBackgroundColor, color: colors[getUserTheme()].mainTextColor }}>
        <div className="settingsMobile-section-wrapper-row" onClick={() => logout(history)}>
          <div>
            <LogoutOutlined className="settingsMobile-section-wrapper-row-icon" />
            <span className="settingsMobile-section-wrapper-row-text">
              <FormattedMessage id={locales.logout.id} />
            </span>
          </div>
        </div>
      </div>

    </div>
  );
};

SettingsMobile.propTypes = {
  history: PropTypes.shape().isRequired,
};

export default SettingsMobile;
