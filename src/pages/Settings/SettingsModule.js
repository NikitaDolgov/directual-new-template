/* eslint-disable react/jsx-props-no-spreading */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Switch, withRouter, Route } from 'react-router';
import { routes } from '../../utils/routes';
import SettingsMobile from './SettingsMobile/SettingsMobile';
import SettingsGeneral from './components/SettinsGeneral/SettingsGeneral';
import SettingsPersonal from './components/SettingsPersonal/SettingsPersonal';


@withRouter
class SettingsModule extends Component {
  state = {
    width: window.innerWidth,
  };

  render() {
    const {
      language, onChangeLanguage, onShowBottomMobile, onChangeTheme, theme,
    } = this.props;
    const { width } = this.state;
    const isMobile = width <= 799;
    if (isMobile) {
      return (
        <Switch>
          <Route
            path={routes.settingsGeneral}
            render={(props) => (
              <SettingsGeneral
                language={language}
                {...props}
                onChangeLanguage={onChangeLanguage}
                onChangeTheme={onChangeTheme}
                theme={theme}
              />
            )}
          />
          <Route
            path={routes.settingsPersonal}
            render={(props) => <SettingsPersonal {...props} onShowBottomMobile={onShowBottomMobile} />}
          />
          <Route exact path={routes.settings} component={SettingsMobile} />
        </Switch>
      );
    }
    return <div />;
  }
}

SettingsModule.propTypes = {
  language: PropTypes.string.isRequired,
  onChangeLanguage: PropTypes.func.isRequired,
  onShowBottomMobile: PropTypes.func.isRequired,
  onChangeTheme: PropTypes.func.isRequired,
  theme: PropTypes.string.isRequired,
};

export default SettingsModule;
