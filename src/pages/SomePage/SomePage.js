import React, { Component } from 'react';
import './SomePage.less';
import underConstraction from '../../assets/images/analytics.svg';


class SomePage extends Component {
  state = {
    width: window.innerWidth,
  };

  render() {
    const { width } = this.state;
    const isMobile = width <= 799;
    if (isMobile) {
      return (
        <div className="somePage-wrapper">
          <img src={underConstraction} alt="under constraction" className="somePage-mobile" />
        </div>
      );
    }
    return <div className="somePage-wrapper" />;
  }
}

export default SomePage;
