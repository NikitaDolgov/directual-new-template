import React, { Component } from 'react';
import {
  Popover, Input, Table, Button,
} from 'antd';
import PropTypes from 'prop-types';
import { QuestionCircleOutlined } from '@ant-design/icons';
import { FormattedMessage, injectIntl } from 'react-intl';
import locales from '../../../../utils/i18n/messages/texts';
import './HomePageDesktop.less';
import getColumns from './utils/getColumns';
import AddUserModal from './components/AddUserModal/AddUserModal';
import EditUserDrawer from './components/EditUserDrawer/EditUserDrawer';
import getUserTheme from '../../../../utils/localeStorage/getUserTheme';
import colors from '../../../../assets/styles/colors';


const { Search } = Input;
class HomePageDesktop extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowAddUserModal: false,
      isShowAddEditUserDrawer: false,
      selectedUser: {},
    };
  }

  onCloseModal = () => this.setState({ isShowAddUserModal: false })

  onSaveModal = (values) => {
    const { onSaveModal } = this.props;
    onSaveModal(values);
    this.setState({ isShowAddUserModal: false });
  }

  onUserSelect = (selectedUser) => {
    this.setState({ selectedUser, isShowAddEditUserDrawer: true });
  }

  onCloseDrawer = () => {
    this.setState({ selectedUser: {}, isShowAddEditUserDrawer: false });
  }

  onSaveDrawer = (values) => {
    const { onSaveDrawer } = this.props;
    onSaveDrawer(values);
    this.setState({ selectedUser: {}, isShowAddEditUserDrawer: false });
  }

  render() {
    const { isShowAddUserModal, selectedUser, isShowAddEditUserDrawer } = this.state;
    const {
      loading, intl, dataSource, onSearch,
    } = this.props;
    return (
      <div className="homePageDesktop-wrapper">
        <div
          className="homePageDesktop-box-wrapper"
          style={{ background: colors[getUserTheme()].elementBackgroundColor }}
        >
          <div>
            <span className="homePageDesktop-box-title">
              <FormattedMessage id={locales.users.id} />
            </span>
            <Popover content={intl.formatMessage({ id: locales.usersPopover.id })} placement="topLeft">
              <QuestionCircleOutlined className="homePageDesktop-icon-question" />
            </Popover>
          </div>
          <div className="homePageDesktop-buttons-wrapper">
            <Search
              placeholder={intl.formatMessage({ id: locales.search.id })}
              onSearch={onSearch}
              style={{ width: 300, alignSelf: 'flex-end' }}
            />
            <Button type="primary" onClick={() => this.setState({ isShowAddUserModal: true })} disabled={loading}>
              <FormattedMessage id={locales.add.id} />
            </Button>
          </div>
          <Table
            dataSource={dataSource}
            columns={getColumns(this.onUserSelect)}
            loading={loading}
            scroll={{ y: '55vh' }}
            pagination={false}
            rowKey="id"
          />
          {isShowAddUserModal && (
          <AddUserModal
            onCloseModal={this.onCloseModal}
            onSaveModal={this.onSaveModal}
          />
          )}
          {isShowAddEditUserDrawer && (
          <EditUserDrawer
            selectedUser={selectedUser}
            onCloseDrawer={this.onCloseDrawer}
            onSaveDrawer={this.onSaveDrawer}
          />
          )}
        </div>
      </div>
    );
  }
}

HomePageDesktop.propTypes = {
  dataSource: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  onSearch: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  intl: PropTypes.shape().isRequired,
  onSaveModal: PropTypes.func.isRequired,
  onSaveDrawer: PropTypes.func.isRequired,
};


export default injectIntl(HomePageDesktop);
