import React from 'react';
import {
  Drawer, Form, Input, Button,
} from 'antd';
import PropTypes from 'prop-types';
import { FormattedMessage, useIntl } from 'react-intl';
import locales from '../../../../../../utils/i18n/messages/texts';


const EditUserDrawer = ({ selectedUser, onCloseDrawer, onSaveDrawer }) => {
  const [form] = Form.useForm();

  const onOk = () => {
    form.submit();
  };

  const intl = useIntl();
  return (
    <Drawer
      title={<FormattedMessage id={locales.editUserDrawerTitle.id} />}
      placement="right"
      width={500}
      destroyOnClose
      closable={false}
      maskClosable={false}
      onClose={onCloseDrawer}
      visible
      footer={(
        <div style={{ textAlign: 'right' }}>
          <Button onClick={onCloseDrawer} style={{ marginRight: 8 }}>
            <FormattedMessage id={locales.cancel.id} />
          </Button>
          <Button onClick={onOk} type="primary">
            <FormattedMessage id={locales.save.id} />
          </Button>
        </div>
      )}
    >
      <Form form={form} layout="vertical" name="editUserForm" onFinish={onSaveDrawer}>
        <Form.Item
          label="ID"
          name="id"
          initialValue={selectedUser.id}
          rules={[{ required: true, message: intl.formatMessage({ id: locales.newUserModalIdPlaceholder.id }) }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label={<FormattedMessage id={locales.firstName.id} />}
          name="firstName"
          initialValue={selectedUser.firstName}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label={<FormattedMessage id={locales.lastName.id} />}
          name="lastName"
          initialValue={selectedUser.lastName}
        >
          <Input />
        </Form.Item>
      </Form>
    </Drawer>
  );
};

EditUserDrawer.propTypes = {
  selectedUser: PropTypes.shape().isRequired,
  onCloseDrawer: PropTypes.func.isRequired,
  onSaveDrawer: PropTypes.func.isRequired,
};

export default EditUserDrawer;
