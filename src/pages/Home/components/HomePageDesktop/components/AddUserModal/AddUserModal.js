/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import {
  Input, Modal, Form,
} from 'antd';
import PropTypes from 'prop-types';
import { FormattedMessage, useIntl } from 'react-intl';
import locales from '../../../../../../utils/i18n/messages/texts';


const AddUserModal = ({ onCloseModal, onSaveModal }) => {
  const [form] = Form.useForm();

  const onOk = () => {
    form.submit();
  };

  const intl = useIntl();

  return (
    <Modal
      title={<FormattedMessage id={locales.newUserModalTitle.id} />}
      visible
      onOk={onOk}
      onCancel={onCloseModal}
      cancelText={<FormattedMessage id={locales.cancel.id} />}
      okText={<FormattedMessage id={locales.save.id} />}
    >
      <Form form={form} layout="vertical" name="addUserForm" onFinish={onSaveModal}>
        <Form.Item
          label="ID"
          name="id"
          rules={[{ required: true, message: intl.formatMessage({ id: locales.newUserModalIdPlaceholder.id }) }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label={<FormattedMessage id={locales.firstName.id} />}
          name="firstName"
        >
          <Input />
        </Form.Item>
        <Form.Item
          label={<FormattedMessage id={locales.lastName.id} />}
          name="lastName"
        >
          <Input />
        </Form.Item>
      </Form>
    </Modal>
  );
};

AddUserModal.propTypes = {
  onCloseModal: PropTypes.func.isRequired,
  onSaveModal: PropTypes.func.isRequired,
};

export default AddUserModal;
