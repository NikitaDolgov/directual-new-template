/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from 'react';
import { Popover } from 'antd';
import { QuestionCircleOutlined } from '@ant-design/icons';
import { FormattedMessage } from 'react-intl';
import locales from '../../../../../utils/i18n/messages/texts';


const getColumns = (onSelect) => {
  const columns = [
    {
      title: <div>ID</div>,
      dataIndex: 'id',
      key: 'id',
      render: (text, object) => <a onClick={() => onSelect(object)}>{object.id}</a>,
      width: '30%',
    },
    {
      title:
  <div>
    <FormattedMessage id={locales.tableUserName.id} />
    <Popover content={<FormattedMessage id={locales.tableUserNamePopover.id} />} placement="topLeft">
      <QuestionCircleOutlined
        style={{
          marginLeft: '5px', fontSize: '12px', fontWeight: 'normal', position: 'absolute',
        }}
      />
    </Popover>
  </div>,
      dataIndex: 'name',
      key: 'name',
    },
  ];
  return columns;
};

export default getColumns;
