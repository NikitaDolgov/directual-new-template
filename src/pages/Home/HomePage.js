import React, { Component } from 'react';
import { message } from 'antd';
import { getUsers, editUser } from '../../utils/http';
import HomePageDesktop from './components/HomePageDesktop/HomePageDesktop';
import HomePageMobile from './components/HomePageMobile/HomePageMobile';


export default class HomePage extends Component {
  state = {
    loading: true,
    data: [],
    originalData: [],
    width: window.innerWidth,
  }

  componentDidMount() {
    getUsers()
      .then((data) => this.setState({ loading: false, data, originalData: data }))
      .catch((error) => message.error(error.message));
  }

  onSearch = (value) => {
    if (value === '') {
      this.setState((prevState) => ({ data: prevState.originalData }));
    } else {
      this.setState((prevState) => ({
        data: prevState.originalData
          .filter((element) => element.name.toLowerCase().indexOf(value.toLowerCase()) >= 0
          || element.id.toLowerCase().indexOf(value.toLowerCase()) >= 0),
      }));
    }
  }

  onSaveModal = (values) => {
    this.setState({ loading: true });
    editUser(values)
      .then((user) => this.setState((prevState) => ({
        loading: false,
        data: [...prevState.data, user],
        originalData: [...prevState.originalData, user],
      })));
  }

  onSaveDrawer = (values) => {
    this.setState({ loading: true });
    editUser(values)
      .then((user) => this.setState((prevState) => ({
        loading: false,
        data: prevState.data.map((u) => (u.id === user.id ? user : u)),
        originalData: prevState.originalData.map((u) => (u.id === user.id ? user : u)),
      })));
  }

  render() {
    const { loading, data, width } = this.state;
    const isMobile = width <= 799;
    if (isMobile) {
      return <HomePageMobile />;
    }
    return (
      <HomePageDesktop
        dataSource={data}
        loading={loading}
        onSearch={this.onSearch}
        onSaveModal={this.onSaveModal}
        onSaveDrawer={this.onSaveDrawer}
      />
    );
  }
}
