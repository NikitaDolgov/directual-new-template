/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from 'react';
import PropTypes from 'prop-types';
import './SideBarButton.less';
import getUserTheme from '../../../../utils/localeStorage/getUserTheme';
import colors from '../../../../assets/styles/colors';


const sideBarButton = ({
  icon, onClick, route, history,
}) => {
  function getButtonStyle(isActive) {
    const theme = getUserTheme();
    if (isActive) {
      return {
        background: colors[theme].primaryColor,
        boxShadow: '-2px 3px 24px 2px rgba(0,0,0,0.45)',
      };
    }
    return {
      background: colors[theme].elementBackgroundColor,
    };
  }

  function getIconStyle(isActive) {
    const theme = getUserTheme();
    if (isActive) {
      return {
        color: theme === 'dark' ? colors.dark.mainTextColor : colors.white,
      };
    }
    return {
      color: colors[theme].secondaryTextColor,
    };
  }

  return (
    <div
      className="sideBarButton-wrapper"
      onClick={() => onClick(route)}
    >
      <div
        className="sideBarButton-wrapper-box"
        style={getButtonStyle(route.includes(history.location.pathname.split('/')[1]))}
      >
        <div
          className="sideBarButton-icon"
          style={getIconStyle(route.includes(history.location.pathname.split('/')[1]))}
        >
          {icon}
        </div>
      </div>
    </div>
  );
};

sideBarButton.propTypes = {
  icon: PropTypes.shape().isRequired,
  onClick: PropTypes.func.isRequired,
  route: PropTypes.string.isRequired,
  history: PropTypes.shape().isRequired,
};

export default sideBarButton;
