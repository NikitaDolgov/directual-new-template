import React from 'react';
import {
  DashboardOutlined,
  DatabaseOutlined,
  ThunderboltOutlined,
  LaptopOutlined,
  HistoryOutlined,
  HomeOutlined,
} from '@ant-design/icons';
import { routes } from '../../../utils/routes';


const buttonsArray = [
  {
    name: 'Dashboard',
    id: 'dashboard',
    icon: <DashboardOutlined />,
    route: routes.homePage,
  },
  {
    name: 'Holdings',
    id: 'holdings',
    icon: <DatabaseOutlined />,
    route: routes.page1,
  },
  {
    name: 'Opportunities',
    id: 'opportunities',
    icon: <ThunderboltOutlined />,
    route: routes.page2,
  },
  {
    name: 'Administration',
    id: 'administration',
    icon: <LaptopOutlined />,
    route: routes.page3,
  },
  {
    name: 'Transactions',
    id: 'transactions',
    icon: <HistoryOutlined />,
    route: routes.page4,
  },
  {
    name: 'Entries',
    id: 'mainPage',
    icon: <HomeOutlined />,
    route: routes.page5,
  },
];

export default buttonsArray;
