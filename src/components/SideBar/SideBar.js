/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import './SideBar.less';
import SideBarButton from './components/SideBarButton/SideBarButton';
import buttonsArray from './utils/buttonsArray';
import { routes } from '../../utils/routes';
import getUserTheme from '../../utils/localeStorage/getUserTheme';
import colors from '../../assets/styles/colors';


@withRouter
class SideBar extends Component {
  handleButtonClick = (route) => {
    const { history } = this.props;
    history.push(route);
  }

  render() {
    const { history } = this.props;
    const { pathname } = history.location;
    const isVisible = pathname !== routes.login;
    if (!isVisible) {
      return null;
    }
    return (
      <div className="sideBar-wrapper" style={{ background: colors[getUserTheme()].elementBackgroundColor }}>
        <div className="sideBar-navigation">
          {buttonsArray
            .map((button) => (
              <SideBarButton
                key={button.id}
                icon={button.icon}
                name={button.name}
                onClick={this.handleButtonClick}
                route={button.route}
                history={history}
              />
            ))}
        </div>
      </div>
    );
  }
}

SideBar.propTypes = {
  history: PropTypes.shape(),
};

SideBar.defaultProps = {
  history: {},
};

export default SideBar;
