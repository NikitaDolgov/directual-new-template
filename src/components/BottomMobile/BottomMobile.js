/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { Component } from 'react';
import { Badge } from 'antd';
import { get } from 'lodash';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import {
  DashboardOutlined,
  ThunderboltOutlined,
  SettingOutlined,
} from '@ant-design/icons';
import { routes } from '../../utils/routes';
import getUserTheme from '../../utils/localeStorage/getUserTheme';
import colors from '../../assets/styles/colors';
import './BottomMobile.less';


@withRouter
class BottomMobile extends Component {
  getButtonStyle = (isActive) => {
    const theme = getUserTheme();
    if (isActive) {
      return {
        background: colors[theme].primaryColor,
        boxShadow: '-2px 3px 24px 2px rgba(0,0,0,0.45)',
      };
    }
    return {
      background: colors[theme].elementBackgroundColor,
    };
  }

  getIconStyle = (isActive) => {
    const theme = getUserTheme();
    if (isActive) {
      return {
        color: theme === 'dark' ? colors.dark.mainTextColor : colors.white,
      };
    }
    return {
      color: colors[theme].secondaryTextColor,
    };
  }

  render() {
    const { history } = this.props;
    const { pathname } = get(history, 'location', '');
    const isVisible = pathname !== routes.login;
    if (!isVisible) {
      return null;
    }
    return (
      <div className={`bottomMobile-wrapper ${getUserTheme()}`}>
        <div
          className="bottomMobile-icon-wrapper"
          style={this.getButtonStyle(pathname === routes.homePage)}
          onClick={() => history.push(routes.homePage)}
        >
          <DashboardOutlined className="bottomMobile-icon" style={this.getIconStyle(pathname === routes.homePage)} />
        </div>

        <div
          className="bottomMobile-icon-wrapper"
          style={this.getButtonStyle(pathname === routes.page1)}
          onClick={() => history.push(routes.page1)}
        >
          <ThunderboltOutlined className="bottomMobile-icon" style={this.getIconStyle(pathname === routes.page1)} />
        </div>

        <div
          className="bottomMobile-icon-wrapper"
          style={this.getButtonStyle(pathname === routes.settings)}
          onClick={() => history.push(routes.settings)}
        >
          <Badge dot={pathname !== routes.settings} offset={[-3, 0]}>
            <SettingOutlined className="bottomMobile-icon" style={this.getIconStyle(pathname === routes.settings)} />
          </Badge>
        </div>

      </div>
    );
  }
}

BottomMobile.propTypes = {
  history: PropTypes.shape(),
};

BottomMobile.defaultProps = {
  history: {},
};

export default BottomMobile;
