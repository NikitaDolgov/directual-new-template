/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable react/no-this-in-sfc */
import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import getUserTheme from '../../utils/localeStorage/getUserTheme';
import colors from '../../assets/styles/colors';
import locales from '../../utils/i18n/messages/texts';
import './FileUpload.less';


const FileUpload = ({ visible, onFileChange, onCancel }) => (
  <div>
    <div
      className="fileUpload-drawer-mask"
      style={{
        transform: visible ? `translateY(${'0vh'})` : `translateY(${'100vh'})`,
        paddingBottom: window.matchMedia('(display-mode: standalone)').matches ? '' : '16vh',
      }}
    >
      <div
        className="fileUpload-drawer-menu"
        style={{
          transform: visible ? `translateY(${'0vh'})` : `translateY(${'100vh'})`,
        }}
      >
        <div
          className="fileUpload-section-wrapper"
          style={{
            backgroundColor: colors[getUserTheme()].elementBackgroundColor,
            color: colors[getUserTheme()].mainTextColor,
          }}
        >
          <div className="fileUpload-section-wrapper-row">
            <label htmlFor="files">
              <input
                id="files"
                style={{ visibility: 'hidden', height: 0 }}
                type="file"
                accept="image/*,image/jpeg"
                onChange={(e) => onFileChange(e.target.files[0])}
              />
              <FormattedMessage id={locales.selectImage.id} />
            </label>
          </div>
          <div className="fileUpload-card-line" />
          <div className="fileUpload-section-wrapper-row" style={{ color: '#c80000' }} onClick={() => onFileChange('')}>
            <FormattedMessage id={locales.removeImage.id} />
          </div>
        </div>
      </div>
      <div className="fileUpload-drawer-menu">
        <div
          className="fileUpload-section-wrapper"
          style={{
            backgroundColor: colors[getUserTheme()].elementBackgroundColor,
            color: colors[getUserTheme()].mainTextColor,
            marginTop: '10px',
          }}
        >
          <div className="fileUpload-section-wrapper-row" onClick={onCancel}>
            <FormattedMessage id={locales.cancel.id} />
          </div>
        </div>
      </div>
    </div>
    <div
      className="fileUpload-black-mask"
      style={{
        opacity: visible ? 0.5 : 0,
        zIndex: visible ? 100 : -1,
      }}
    />
  </div>
);

FileUpload.propTypes = {
  visible: PropTypes.bool.isRequired,
  onCancel: PropTypes.func.isRequired,
  onFileChange: PropTypes.func.isRequired,
};


export default FileUpload;
