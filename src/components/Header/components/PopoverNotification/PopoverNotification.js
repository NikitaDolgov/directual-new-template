import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Notification from '../Notification/Notification';
import './PopoverNotification.css';


class PopoverNotification extends Component {
  render() {
    const { isFilterSelected, notifications } = this.props;
    return (
      <div className="popoverNotification-wrapper">
        {notifications.length === 0
          && <span style={{ margin: 'auto' }}>No new notifications</span>}
        {notifications
          .filter((notification) => (isFilterSelected ? !notification.isViewed : true))
          .map((notification, index) => (
            <Notification
              key={notification.id}
              notification={notification}
              // toggle={() => toggle(notification.id)}
              isLastElement={notifications.length === index + 1}
            />
          ))}
      </div>
    );
  }
}

PopoverNotification.propTypes = {
  isFilterSelected: PropTypes.bool.isRequired,
  notifications: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  // toggle: PropTypes.func.isRequired,
};

export default PopoverNotification;
