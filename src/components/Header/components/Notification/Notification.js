import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import './Notification.css';


class Notification extends Component {
  static propTypes = {
    notification: PropTypes.shape().isRequired,
    // toggle: PropTypes.func.isRequired,
    isLastElement: PropTypes.bool.isRequired,
  }

  render() {
    const { notification, isLastElement } = this.props;
    return (
      <div
        className="notification-wrapper"
        style={{
          backgroundColor: notification.isViewed ? 'transparent' : 'rgb(226 235 251)',
          borderBottom: isLastElement ? 'none' : '1px solid #d9d9d9',
          // borderRadius: isLastElement ? '0px 0px 12px 12px' : 'none',
        }}
      >
        <div className="notification-title-wrapper">
          <span className="notification-title">{notification.title}</span>
          <div
            className="notification-icon"
            style={{ backgroundColor: notification.isViewed ? 'transparent' : '#4285f4' }}
            // onClick={toggle}
          />
        </div>
        <span className="notification-description">{notification.description}</span>
        <div className="notification-date-wrapper"><span className="notification-date" >{moment(notification.date).subtract(3, 'hours').calendar(null, {
          sameElse: 'DD MMMM YYYY',
        })}</span></div>
      </div>);
  }
}

export default Notification;
