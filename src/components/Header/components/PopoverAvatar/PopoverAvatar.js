/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage, injectIntl } from 'react-intl';
import {
  Avatar, Form, Input, Modal, DatePicker, Popconfirm, Upload, message, notification,
} from 'antd';
import md5 from 'md5';
import {
  SettingOutlined,
  LogoutOutlined,
  MessageOutlined,
  UserOutlined,
  DeleteOutlined,
  EditOutlined,
} from '@ant-design/icons';
import moment from 'moment';
import InputMask from 'react-input-mask';
import locales from '../../../../utils/i18n/messages/texts';
import './PopoverAvatar.css';
import logout from '../../../../utils/logout';
import getUserTheme from '../../../../utils/localeStorage/getUserTheme';
import colors from '../../../../assets/styles/colors';
import getUserLogo from '../../../../utils/localeStorage/getUserLogo';
import getUserId from '../../../../utils/localeStorage/getUserId';
import getUserFirstName from '../../../../utils/localeStorage/getUserFirstName';
import getUserLastName from '../../../../utils/localeStorage/getUserLastName';
import getUserBirthdayDate from '../../../../utils/localeStorage/getUserBirthdayDate';
import getUserPhone from '../../../../utils/localeStorage/getUserPhone';
import getUserEmail from '../../../../utils/localeStorage/getUserEmail';
import getUserMiddleName from '../../../../utils/localeStorage/getUserMiddleName';
import { editUser } from '../../../../utils/http';
import { setLocalStorage as setStorage } from '../../../../utils/localeStorage/storage';
import api from '../../../../utils/directualAPI/api';


class PopoverAvatar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowSettingsModal: false,
      isHoveringEdit: false,
      isHoveringDelete: false,
      loading: false,
    };
    this.formRef = React.createRef();
  }

  handleMouseHover = (field) => {
    this.setState((state) => this.toggleHoverState(state, field));
  }

  handleCancel = () => {
    this.setState({
      isShowSettingsModal: false,
    });
  }

  openNotification = () => {
    const { handleVisibleChange, intl } = this.props;
    const args = {
      message: intl.formatMessage({ id: locales.help.id }),
      description: intl.formatMessage({ id: locales.helpNotification.id }),
      duration: 5,
    };
    notification.open(args);
    handleVisibleChange(false);
  };

  onSubmit = async (values) => {
    const { intl } = this.props;
    this.setState({ loading: true });
    if ((values.oldPassword && !values.newPassword) || (!values.oldPassword && values.newPassword)) {
      message.error(intl.formatMessage({ id: locales.passwordEditError.id }));
      this.setState({ loading: false });
      return;
    }

    let isPasswordOK = true;
    if (values.oldPassword && values.newPassword) {
      await api.structure().auth(getUserId(), values.oldPassword)
        .then((response) => {
          if (response.isError) {
            isPasswordOK = false;
            if (response.isPassError) {
              message.error(intl.formatMessage({ id: locales.passwordError.id }));
            } else {
              message.error(response.token);
            }
          }
        });
    }
    if (isPasswordOK) {
      const body = {
        id: getUserId(),
        ...values,
        password: md5(values.newPassword),
        birthdayDate: values.birthdayDate ? values.birthdayDate.toISOString() : null,
      };
      delete body.oldPassword;
      delete body.newPassword;
      await editUser(body)
        .then(() => {
          setStorage(body);
          message.success(intl.formatMessage({ id: locales.success.id }));
        })
        .catch((error) => {
          message.error(error.message);
        });
    }
    this.setState({ loading: false });
  }

  onOk = () => {
    this.formRef.current.submit();
  }

  onLogoChanges = (info) => {
    this.setState({ loading: true });
    const headers = {
      'Content-Type': 'multipart/form-data',
    };
    const form = new FormData();
    form.append('id', getUserId());
    form.append('logo_id', info.file.originFileObj);
    editUser(form, headers)
      .then((user) => {
        setStorage({ logo: user.logoURL });
        this.setState({ loading: false });
      })
      .catch((error) => {
        this.setState({ loading: false });
        message.error(error.message);
      });
  }

  showSettingsModal = () => {
    const { handleVisibleChange } = this.props;
    handleVisibleChange(false);
    this.setState({ isShowSettingsModal: true });
  }

  toggleHoverState(state, field) {
    return {
      [field]: !state[field],
    };
  }

  render() {
    const {
      isShowSettingsModal, isHoveringDelete, isHoveringEdit, loading,
    } = this.state;
    const { history } = this.props;
    return (
      <div className="popoverAvatar-wrapper">
        <div>
          {getUserLogo()
            ? <Avatar src={getUserLogo()} size={80} />
            : <Avatar style={{ backgroundColor: colors[getUserTheme()].primaryColor }} icon={<UserOutlined />} size={80} />}
        </div>
        <div className="popoverAvatar-button-wrapper">
          <span className="popoverAvatar-name">{`${getUserLastName() || '***'} ${getUserFirstName() || '***'}`}</span>
        </div>
        <div className="popoverAvatar-button" onClick={this.openNotification}>
          <MessageOutlined className="settingsMobile-section-wrapper-row-icon" />
          <FormattedMessage id={locales.help.id} />
        </div>
        <div className="popoverAvatar-button" onClick={this.showSettingsModal}>
          <SettingOutlined className="settingsMobile-section-wrapper-row-icon" />
          <FormattedMessage id={locales.settings.id} />
        </div>
        <div className="popoverAvatar-button" onClick={() => logout(history)}>
          <LogoutOutlined className="settingsMobile-section-wrapper-row-icon" />
          <FormattedMessage id={locales.logout.id} />
        </div>
        <Modal
          title={<FormattedMessage id={locales.userProfile.id} />}
          visible={isShowSettingsModal}
          onOk={this.onOk}
          onCancel={this.handleCancel}
          cancelText={<FormattedMessage id={locales.cancel.id} />}
          okText={<FormattedMessage id={locales.save.id} />}
          okButtonProps={{ disabled: loading }}
          cancelButtonProps={{ disabled: loading }}
        >
          <Form
            initialValues={{ remember: true }}
            onFinish={this.onSubmit}
            ref={this.formRef}
            labelCol={{ span: 10 }}
            wrapperCol={{ span: 16 }}
          >
            <div className="avatar-settings-content-wrapper">
              <div className="popoverAvatar-uploader">
                {getUserLogo()
                  ? <Avatar src={getUserLogo()} size={150} />
                  : <Avatar style={{ backgroundColor: colors[getUserTheme()].primaryColor }} icon={<UserOutlined />} size={150} />}
                <Upload
                  showUploadList={false}
                  onChange={this.onLogoChanges}
                >
                  <div
                    className="popoverAvatar-uploader-edit"
                    style={{ opacity: isHoveringEdit ? '0.7' : '0' }}
                    onMouseEnter={() => this.handleMouseHover('isHoveringEdit')}
                    onMouseLeave={() => this.handleMouseHover('isHoveringEdit')}
                  >
                    <EditOutlined style={{ fontSize: '20px' }} />
                  </div>
                </Upload>
                <Popconfirm
                  title="Are you sure you want to delete?"
                  onConfirm={this.deleteLogo}
                  okText="Yes"
                  cancelText="No"
                >
                  <div
                    className="popoverAvatar-uploader-delete"
                    style={{ opacity: isHoveringDelete ? '0.7' : '0' }}
                    onMouseEnter={() => this.handleMouseHover('isHoveringDelete')}
                    onMouseLeave={() => this.handleMouseHover('isHoveringDelete')}
                  >
                    <DeleteOutlined style={{ fontSize: '20px' }} />
                  </div>
                </Popconfirm>
              </div>
              <Form.Item
                label={<FormattedMessage id={locales.lastName.id} />}
                name="lastName"
                initialValue={getUserLastName()}
              >
                <Input disabled={loading} />
              </Form.Item>
              <Form.Item
                label={<FormattedMessage id={locales.firstName.id} />}
                name="firstName"
                initialValue={getUserFirstName()}
              >
                <Input disabled={loading} />
              </Form.Item>
              <Form.Item
                label={<FormattedMessage id={locales.middleName.id} />}
                name="middleName"
                initialValue={getUserMiddleName()}
              >
                <Input disabled={loading} />
              </Form.Item>
              <Form.Item
                label={<FormattedMessage id={locales.birthdayDate.id} />}
                name="birthdayDate"
                initialValue={getUserBirthdayDate() ? moment(getUserBirthdayDate()) : null}
              >
                <DatePicker disabled={loading} />
              </Form.Item>
              <Form.Item
                label={<FormattedMessage id={locales.email.id} />}
                name="email"
                initialValue={getUserEmail()}
              >
                <Input disabled={loading} />
              </Form.Item>
              <Form.Item
                label={<FormattedMessage id={locales.phone.id} />}
                name="phone"
                initialValue={getUserPhone()}
              >
                <InputMask
                  mask="+7 (999) 999-99-99"
                  maskChar=" "
                  className="popoverAvatar-phone-input"
                  inputMode="numeric"
                  disabled={loading}
                />
              </Form.Item>
              <Form.Item
                label={<FormattedMessage id={locales.oldPassword.id} />}
                name="oldPassword"
              >
                <Input.Password disabled={loading} />
              </Form.Item>
              <Form.Item
                label={<FormattedMessage id={locales.newPassword.id} />}
                name="newPassword"
              >
                <Input.Password disabled={loading} />
              </Form.Item>
            </div>
          </Form>
        </Modal>
      </div>
    );
  }
}

PopoverAvatar.propTypes = {
  history: PropTypes.shape().isRequired,
  handleVisibleChange: PropTypes.func.isRequired,
  intl: PropTypes.shape().isRequired,
};

export default injectIntl(PopoverAvatar);
