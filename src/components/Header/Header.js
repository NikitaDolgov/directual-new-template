import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import { get } from 'lodash';
import { Popover, Badge, Avatar } from 'antd';
import { SearchOutlined, BellOutlined, UserOutlined } from '@ant-design/icons';
import getUserLogo from '../../utils/localeStorage/getUserLogo';
import { routes } from '../../utils/routes';
import getUserTheme from '../../utils/localeStorage/getUserTheme';
import colors from '../../assets/styles/colors';
import './Header.less';
import PopoverAvatarContent from './components/PopoverAvatar/PopoverAvatar';


@withRouter
class Header extends Component {
  state = {
    isPopoverVisible: false,
  }

  handleVisibleChange = (visible) => {
    this.setState({ isPopoverVisible: visible });
  };

  render() {
    const { isPopoverVisible } = this.state;
    const { history } = this.props;
    const pathname = get(history, 'location.pathname', '');
    const isVisible = pathname !== routes.login;
    if (!isVisible) {
      return null;
    }
    return (
      <div className="header-wrapper" style={{ background: colors[getUserTheme()].elementBackgroundColor }}>
        <div className="header-top-icons">
          <div className="header-top-icon-wrapper">
            <SearchOutlined className="header-icon" />
          </div>
          <Popover
            placement="bottomLeft"
            title={(<div />
              // <div className="header-popover-title">
              //   <span className="header-popover-title-text">Notifications</span>
              //   <FilterOutlined
              //     onClick={() => {}}
              //     className="header-popover-title-icon"
              //   />
              // </div>
            )}
            content={(<div />
              // <PopoverContent
              //   isFilterSelected={isFilterSelected}
              //   notifications={notifications}
              // />
            )}
            trigger="click"
            overlayClassName="header-popover"
          >
            <div className="header-top-icon-wrapper">
              <Badge count={1} overflowCount={9} offset={[-4, 3]} className="header-badge">
                <BellOutlined className="header-icon" />
              </Badge>
            </div>
          </Popover>
          <Popover
            placement="bottomLeft"
            content={<PopoverAvatarContent handleVisibleChange={this.handleVisibleChange} history={history} />}
            trigger="click"
            overlayClassName="header-popover"
            visible={isPopoverVisible}
            onVisibleChange={this.handleVisibleChange}
          >
            <div className="header-top-icon-wrapper">
              {getUserLogo()
                ? <Avatar src={getUserLogo()} size={40} />
                : <Avatar style={{ backgroundColor: '#c80201' }} icon={<UserOutlined />} size={40} />}
            </div>
          </Popover>
        </div>
      </div>
    );
  }
}

Header.propTypes = {
  history: PropTypes.shape(),
};

Header.defaultProps = {
  history: {},
};

export default Header;
