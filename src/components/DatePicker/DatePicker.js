/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { FormattedMessage } from 'react-intl';
import locales from '../../utils/i18n/messages/texts';
import './DatePicker.less';
import getUserTheme from '../../utils/localeStorage/getUserTheme';
import colors from '../../assets/styles/colors';


class DatePicker extends Component {
  constructor(props) {
    super(props);
    this.scrollDate = React.createRef();
    this.scrollMonth = React.createRef();
    this.scrollYear = React.createRef();
    this.state = {
      scrollDate: 0,
      scrollMonth: 0,
      scrollYear: 0,
      years: [],
      months: [],
      days: [],
    };
  }

  componentDidMount() {
    const { initialValue } = this.props;
    const year = (new Date()).getFullYear() + 50;
    const years = Array.from(new Array(100), (val, index) => year - index);
    const months = [];
    const days = [];
    for (let i = 1; i < 13; i += 1) {
      months.push(moment(i, 'M').format('MMMM'));
    }
    for (let i = 1; i < 32; i += 1) {
      days.push(i);
    }
    setTimeout(() => {
      this.scrollDate.current.scrollTop = (initialValue.date() - 1) * 30;
      this.scrollMonth.current.scrollTop = initialValue.month() * 30;
      this.scrollYear.current.scrollTop = years.indexOf(initialValue.year()) * 30;
    }, 1000);
    this.setState({
      years: [null, null, ...years, null, null],
      months: [null, null, ...months, null, null],
      days: [null, null, ...days, null, null],
    });
  }

  onScroll = (ref) => {
    const { scrollTop } = this[ref].current || { scrollTop: 0 };
    this.setState({ [ref]: scrollTop });
  }

  onConfirm = () => {
    const { onConfirm } = this.props;
    const {
      scrollDate, scrollMonth, scrollYear, years,
    } = this.state;
    const date = moment(`${(scrollDate / 30) + 1} ${(scrollMonth / 30) + 1} ${years[(scrollYear / 30) + 2]}`, 'DD MM YYYY');
    onConfirm(date);
  }

  onDateClick = (value) => {
    this.scrollDate.current.scrollTop = (value - 1) * 30;
  }

  onMonthClick = (value) => {
    this.scrollMonth.current.scrollTop = (moment().month(value).format('M') - 1) * 30;
  }

  onYearClick = (value) => {
    const year = (new Date()).getFullYear() + 50;
    const years = Array.from(new Array(100), (val, index) => year - index);
    this.scrollYear.current.scrollTop = years.indexOf(value) * 30;
  }

  render() {
    const {
      years, months, days, scrollDate, scrollMonth, scrollYear,
    } = this.state;
    const selectedDate = scrollDate / 30 + 1;
    const selectedMonth = scrollMonth / 30 + 1;
    const selectedYear = years[Math.round(scrollYear / 30) + 2];
    const { visible, onCancel } = this.props;
    return (
      <div>
        <div
          className="datePicker-drawer-mask"
          style={{
            transform: visible ? `translateY(${'0vh'})` : `translateY(${'100vh'})`,
            paddingBottom: window.matchMedia('(display-mode: standalone)').matches ? '' : '16vh',
          }}
        >
          <div
            className="datePicker-drawer-menu"
            style={{
              transform: visible ? `translateY(${'0vh'})` : `translateY(${'100vh'})`,
            }}
          >
            <div
              className="datePicker-section-wrapper"
              style={{
                backgroundColor: colors[getUserTheme()].elementBackgroundColor,
                color: colors[getUserTheme()].mainTextColor,
              }}
            >
              <div className="datePicker-section-wrapper-row">
                <FormattedMessage id={locales.pickADate.id} />
              </div>
              <div className="datePicker-card-line" style={{ width: '100%' }} />
              <div className="datePicker-drawer-main-wrapper">
                <div
                  className="datePicker-drawer-main-block-wrapper"
                  ref={this.scrollDate}
                  onScroll={() => this.onScroll('scrollDate')}
                >
                  {days.map((e) => (
                    <div
                      key={e + new Date() + Math.random()}
                      className="datePicker-drawer-main-block-row"
                      onClick={() => this.onDateClick(e)}
                    >
                      <div
                        style={{
                          opacity: (Math.abs(e - selectedDate) * (-1) + 3) * 0.33,
                          transform: `scale(${1 - Math.abs(e - selectedDate) * 0.2},${1 - Math.abs(e - selectedDate) * 0.2}) skewX(${(e - selectedDate) * 5}deg)`,
                          marginRight: Math.abs(e - selectedDate) === 2 ? '-5px' : '0px',
                        }}
                      >
                        {e || ''}
                      </div>
                    </div>
                  ))}
                </div>
                <div
                  className="datePicker-drawer-main-block-wrapper"
                  ref={this.scrollMonth}
                  onScroll={() => this.onScroll('scrollMonth')}
                >
                  {months.map((e) => (
                    <div
                      key={e + new Date() + Math.random()}
                      className="datePicker-drawer-main-block-row"
                      onClick={() => this.onMonthClick(e)}
                      style={{ perspective: '100px' }}
                    >
                      <div
                        style={{
                          transform: `rotateX(${(moment().month(e || 'январь').format('M') - selectedMonth) * (-1)}5deg)`,
                          opacity: (Math.abs(moment().month(e || 'январь').format('M') - selectedMonth) * (-1) + 3) * 0.33,
                        }}
                      >
                        {e || ''}
                      </div>
                    </div>
                  ))}
                </div>
                <div
                  className="datePicker-drawer-main-block-wrapper"
                  ref={this.scrollYear}
                  onScroll={() => this.onScroll('scrollYear')}
                >
                  {years.map((e) => (
                    <div
                      key={e + new Date() + Math.random()}
                      className="datePicker-drawer-main-block-row"
                      onClick={() => this.onYearClick(e)}
                    >
                      <div
                        style={{
                          transform: `scale(${1 - Math.abs(e - selectedYear) * 0.2},${1 - Math.abs(e - selectedYear) * 0.2}) skewX(${(e - selectedYear) * 5}deg)`,
                          opacity: (Math.abs(e - selectedYear) * (-1) + 3) * 0.33,
                          marginLeft: `${Math.abs(e - selectedYear) * -5}px`,
                        }}
                      >
                        {e || ''}
                      </div>
                    </div>
                  ))}
                </div>
                <div style={{
                  position: 'absolute', height: '30px', border: '1px solid #e9e9e9', width: '100%', top: '40%',
                }}
                />
              </div>
              <div className="datePicker-card-line" style={{ width: '100%' }} />
              <div className="datePicker-section-wrapper-row" onClick={this.onConfirm} style={{ fontWeight: 'bold' }}>
                <FormattedMessage id={locales.done.id} />
              </div>
            </div>
          </div>
          <div className="datePicker-drawer-menu">
            <div
              className="datePicker-section-wrapper"
              style={{
                backgroundColor: colors[getUserTheme()].elementBackgroundColor,
                color: colors[getUserTheme()].mainTextColor,
                marginTop: '10px',
              }}
            >
              <div className="datePicker-section-wrapper-row" onClick={onCancel}>
                <FormattedMessage id={locales.cancel.id} />
              </div>
            </div>
          </div>
        </div>
        <div
          className="datePicker-black-mask"
          style={{
            opacity: visible ? 0.5 : 0,
            zIndex: visible ? 100 : -1,
          }}
        />
      </div>
    );
  }
}

DatePicker.propTypes = {
  visible: PropTypes.bool.isRequired,
  onConfirm: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  initialValue: PropTypes.shape().isRequired,
};

export default DatePicker;
