import React, { Component } from 'react';
import moment from 'moment';
import ru from 'moment/locale/ru';
import { ConfigProvider, Radio } from 'antd';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
import ru_RU from 'antd/es/locale/ru_RU';
import PrivateRoute from './utils/privateRoute';
import LoginPage from './pages/Login/LoginPage';
import HomePage from './pages/Home/HomePage';
import { routes } from './utils/routes';
import { I18nProvider, LOCALES } from './utils/i18n';
import SideBar from './components/SideBar/SideBar';
import Header from './components/Header/Header';
import BottomMobile from './components/BottomMobile/BottomMobile';
import Settings from './pages/Settings/SettingsModule';
import SomePage from './pages/SomePage/SomePage';
import { setLocalStorage as setStorage } from './utils/localeStorage/storage';
import getUserTheme from './utils/localeStorage/getUserTheme';
import colors from './assets/styles/colors';


moment.updateLocale('ru', ru);
export default class App extends Component {
  state = {
    language: LOCALES.RUSSIAN,
    theme: getUserTheme() || 'light',
    width: window.innerWidth,
    isShowBottomMobile: true,
  };

  onChangeLanguage = (language) => {
    this.setState({ language });
  }

  onChangeTheme = (theme) => {
    this.setState({ theme });
    setStorage({ theme });
  }

  onShowBottomMobile = (isShowBottomMobile) => {
    this.setState({ isShowBottomMobile });
  }

  render() {
    const {
      language, width, isShowBottomMobile, theme,
    } = this.state;
    const isMobile = width <= 799;
    const isPWA = window.matchMedia('(display-mode: standalone)').matches;
    const isBottomVisible = isShowBottomMobile && isMobile;
    return (
      <I18nProvider locale={language}>
        <ConfigProvider locale={ru_RU}>
          <Router>
            {!isMobile && (
            <div className="locale-button-wrapper">
              <Radio.Group
                defaultValue={language}
                buttonStyle="solid"
                onChange={(e) => this.onChangeLanguage(e.target.value)}
              >
                <Radio.Button value={LOCALES.RUSSIAN}>
                  <span role="img" aria-label="">
                    🇷🇺 Русский
                  </span>
                </Radio.Button>
                <Radio.Button
                  value={LOCALES.ENGLISH}
                >
                  <span role="img" aria-label="">
                    🇬🇧 English
                  </span>
                </Radio.Button>
              </Radio.Group>
            </div>
            )}
            <div style={{
              display: 'flex',
              background: colors[getUserTheme()].mainBackgroundColor,
              color: colors[getUserTheme()].mainTextColor,
              height: isPWA ? '101vh' : '',
            }}
            >
              {!isMobile && <SideBar />}
              <div style={{ width: '100%', background: 'inherit', color: 'inherit' }}>
                {isBottomVisible && <BottomMobile />}
                {!isMobile && <Header />}
                <Switch>
                  <Route path={routes.login}>
                    <LoginPage />
                  </Route>
                  <Redirect exact from={routes.home} to={routes.homePage} />
                  <PrivateRoute path={routes.homePage}>
                    <HomePage />
                  </PrivateRoute>
                  <PrivateRoute path={routes.settings}>
                    <Settings
                      language={language}
                      onChangeLanguage={this.onChangeLanguage}
                      onShowBottomMobile={this.onShowBottomMobile}
                      onChangeTheme={this.onChangeTheme}
                      theme={theme}
                    />
                  </PrivateRoute>
                  <PrivateRoute path={routes.page1}>
                    <SomePage />
                  </PrivateRoute>
                </Switch>
              </div>
            </div>
          </Router>
        </ConfigProvider>
      </I18nProvider>
    );
  }
}
