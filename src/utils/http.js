import { createBrowserHistory as createHistory } from 'history';
import { get } from 'lodash';
import moment from 'moment';
import api from './directualAPI/api';


function handleError(error) {
  if (get(error, 'response.status', null) === 403) {
    return createHistory({ forceRefresh: true }).push('/login');
  }
  throw new Error(error);
}

function parseUser(user) {
  return {
    id: get(user, 'id', null),
    firstName: get(user, 'firstName', ''),
    lastName: get(user, 'lastName', ''),
    middleName: get(user, 'middleName', ''),
    phone: get(user, 'phone', ''),
    email: get(user, 'email', ''),
    birthdayDate: get(user, 'birthdayDate', null) ? moment(get(user, 'birthdayDate', null)) : null,
    name: `${get(user, 'lastName', '')} ${get(user, 'firstName', '')}`,
    logoURL: get(user, 'logo_id.urlLink', null),
    theme: get(user, 'theme', 'light'),
    language: get(user, 'email', 'ru-RU'),
  };
}
export function getUsers() {
  return api
    .structure('WebUser')
    .getData('getUsers', {})
    .then((resp) => get(resp, 'payload', []).map((user) => parseUser(user)))
    .catch(handleError);
}

export function editUser(values, headers) {
  return api
    .structure('WebUser')
    .sendData('editUser', values, headers)
    .then((resp) => parseUser(get(resp, 'result[0]', {})))
    .catch(handleError);
}

export function getUser(userID, sessionID) {
  return api
    .structure('WebUser')
    .getData('getUser', {
      userID,
      sessionID,
    })
    .then((resp) => parseUser(get(resp, 'payload[0]', {})))
    .catch(handleError);
}
