export const routes = {
  home: '/',
  login: '/login',
  homePage: '/homePage',
  page: '/page',
  page1: '/page1',
  page2: '/page2',
  page3: '/page3',
  page4: '/page4',
  page5: '/page5',
  settings: '/settings',
  settingsPersonal: '/settings/personal',
  settingsGeneral: '/settings/general',
};
