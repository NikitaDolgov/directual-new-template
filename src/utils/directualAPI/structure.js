import axios from 'axios';
import { get } from 'lodash';
import getSessionId from '../localeStorage/getSessionId';


export default class Structure {
  constructor(structureName, config) {
    this.structureName = structureName;
    this.config = config;
  }

  getData(endpoint, params, options) {
    return axios.get(`${this.config.apiHost}/good/api/v5/data/${this.structureName}/${endpoint}`, {
      params: {
        appID: this.config.appID,
        sessionID: getSessionId(),
        ...params,
      },
      ...options,
    })
      .then((response) => get(response, 'data', {}));
  }

  sendData(endpoint, data, headers) {
    const request = {
      method: 'post',
      url: `${this.config.apiHost}/good/api/v5/data/${this.structureName}/${endpoint}?appID=${this.config.appID}&sessionID=${getSessionId()}`,
      data,
      headers,
    };
    return axios(request)
      .then((response) => get(response, 'data', {}));
  }

  auth(username, password) {
    return axios.post(`${this.config.apiHost}/good/api/v5/auth?appID=${this.config.appID}`, {
      username, password, appID: this.config.appID, provider: 'rest',
    })
      .then((response) => ({ token: get(response, 'data.result.token', null), isError: false }))
      .catch((error) => {
        if (error.response.status === 400) {
          return ({ data: error.message, isError: true, isPassError: true });
        }
        return ({ data: error.message, isError: true, isPassError: false });
      });
  }
}
