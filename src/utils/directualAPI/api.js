import { config } from '../config';
import Structure from './structure';


export default class Api {
  static structure(structureName) {
    return new Structure(structureName, config);
  }
}
