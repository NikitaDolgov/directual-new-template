import { getLocalStorage as getStorage } from './storage';


export default function getUserFirstName() {
  return getStorage(['firstName']).firstName;
}
