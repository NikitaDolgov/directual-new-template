import { getLocalStorage as getStorage } from './storage';


export default function getUserLastName() {
  return getStorage(['lastName']).lastName;
}
