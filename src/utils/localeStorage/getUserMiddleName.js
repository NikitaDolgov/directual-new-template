import { getLocalStorage as getStorage } from './storage';


export default function getUserMiddleName() {
  return getStorage(['middleName']).middleName;
}
