import { getLocalStorage as getStorage } from './storage';


export default function getUserEmail() {
  return getStorage(['email']).email;
}
