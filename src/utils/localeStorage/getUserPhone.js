import { getLocalStorage as getStorage } from './storage';


export default function getUserPhone() {
  return getStorage(['phone']).phone;
}
