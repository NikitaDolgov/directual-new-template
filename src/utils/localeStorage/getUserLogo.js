import { getLocalStorage as getStorage } from './storage';


export default function getUserLogo() {
  return getStorage(['logo']).logo;
}
