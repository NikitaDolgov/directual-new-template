import { getLocalStorage as getStorage } from './storage';


export default function getSessionId() {
  return getStorage(['token']).token;
}
