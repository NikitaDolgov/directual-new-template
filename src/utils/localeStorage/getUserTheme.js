import { getLocalStorage as getStorage } from './storage';


export default function getUserTheme() {
  return getStorage(['theme']).theme || 'light';
}
