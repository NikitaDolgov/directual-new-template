import { getLocalStorage as getStorage } from './storage';


export default function getUserId() {
  return getStorage(['id']).id;
}
