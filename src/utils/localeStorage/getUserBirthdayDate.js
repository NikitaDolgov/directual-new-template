import { getLocalStorage as getStorage } from './storage';


export default function getUserBirthdayDate() {
  return getStorage(['birthdayDate']).birthdayDate;
}
