import { routes } from './routes';


const logout = (history) => {
  window.localStorage.clear();
  history.push(routes.login);
};

export default logout;
