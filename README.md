# Getting Started with Create React App
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Contact me on telegram @nik_dolgov

**IMPORTANT**\
1. Change APP_ID in src/utils/config.js\
2. This build supports .less\
3. NO SERVER.js\
4. Add it to your mobile' home screen so it will work as mobile app (PWA)\
5. This project is made mostly using Antd components
6. Enable CORS on all API Endpoints in Directual + go to "Api Security settings" and "Enable CORS for auth functions" (type * under "Lists of pages on which the authorization form and/or pages with api caller is located")

## You can host it on:
**Vercel**\
Click "Create new site from Git" and follow the instructions\
**Netlify**\
Click "new Project" -> "Import Git Repository" and follow the instructions\
**Heroku**\
Set Buildpack - mars/create-react-app in Settings on heroku or it will not work

## Docker
Build and tag the Docker image (don't forget the dot!)
> $ docker build -t sample:dev . 

Then, spin up the container once the build is done:
> $ docker run \
    -it \
    --rm \
    -v ${PWD}:/app \
    -v /app/node_modules \
    -p 3001:3000 \
    -e CHOKIDAR_USEPOLLING=true \
    sample:dev

Open your browser to http://localhost:3001/ and you should see the app

## Project Structure
- assets (images, fonts, general styles)
- components (reusable elements + header + sidebar)
- pages (site pages)
- utils (support functions and components to make everything work)

## Localisation
Add new texts to src/utils/i18n/messages/texts.js

## Directual Integration
All http requests can be found in src/utils/http.js

## Custom Theme
You can customize your app in src/assets/styles/custom.less
